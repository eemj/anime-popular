const request = require("request-promise-native");

const newerThan = new Date().setDate(new Date().getDate() - 7) / 1000;

const intervals = [
  { label: "year", seconds: 31536000 },
  { label: "month", seconds: 2592000 },
  { label: "day", seconds: 86400 },
  { label: "hour", seconds: 3600 },
  { label: "minute", seconds: 60 },
  { label: "second", seconds: 0 },
];

const timeSince = (ms) => {
  const seconds = Math.abs(Math.floor(Date.now() / 1000 - ms));
  const interval = intervals.find(i => i.seconds < seconds);
  const count = Math.floor(seconds / interval.seconds);
  return `${count} ${interval.label}${count !== 1 ? "s" : ""} ago`;
};

const getResponse = (after = null) => request.get({
  uri: `https://www.reddit.com/r/anime/search.json?sort=new&q=flair_name%3A%22Episode%22&restrict_sr=1&after=${after}`,
  json: true,
});

const titleExpr = /^(.*) (?:d|D)iscussion/i;

const main = async () => {
  let children = [];

  let lastAfter = null;

  for (let i = 0; i < 3; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    const res = await getResponse(lastAfter);

    const { data } = res;

    children.push(data.children);

    lastAfter = data.after || null;
  }

  // merging the children together
  children = children.reduce((a, b) => a.concat(b));

  // that released on reddit.
  children = children
    .filter(({ data }) => data.created >= newerThan && titleExpr.test(data.title))
    .map(({ data }) => ({
      upvotes: data.score,
      title: data.title.match(titleExpr)[1],
      comments: data.num_comments,
      score: Math.round((data.score + data.num_comments) / 2),
      created: timeSince(data.created),
    }));

  // removing duplicates
  children = children.filter(
    (child, index, self) => index === self.findIndex(c => c.title === child.title),
  );

  // and finally sorting the animes with the most comments.
  children = children.sort((a, b) => b.score - a.score);

  console.table(children);
};

main();
